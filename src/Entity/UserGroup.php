<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserGroupRepository")
 */
class UserGroup
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="userGroups")
     * @ORM\JoinColumn(nullable=false)
     */
    private $owner;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdDateTime;

    /**
     * @ORM\Column(type="integer")
     */
    private $userCount;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserGroupJoinRequest", mappedBy="userGroup")
     */
    private $userGroupJoinRequests;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserGroupUser", mappedBy="userGroup")
     */
    private $userGroupUsers;

    public function __construct()
    {
        $this->userGroupUsers = new ArrayCollection();
        $this->userGroupJoinRequests = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(?User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    public function getCreatedDateTime(): ?\DateTimeInterface
    {
        return $this->createdDateTime;
    }

    public function setCreatedDateTime(\DateTimeInterface $createdDateTime): self
    {
        $this->createdDateTime = $createdDateTime;

        return $this;
    }

    public function getUserCount(): ?int
    {
        return $this->userCount;
    }

    public function setUserCount(int $userCount): self
    {
        $this->userCount = $userCount;

        return $this;
    }

    /**
     * @return Collection|UserGroupJoinRequest[]
     */
    public function getUserGroupJoinRequests(): Collection
    {
        return $this->userGroupJoinRequests;
    }

    public function addUserGroupJoinRequest(UserGroupJoinRequest $userGroupJoinRequest): self
    {
        if (!$this->userGroupJoinRequests->contains($userGroupJoinRequest)) {
            $this->userGroupJoinRequests[] = $userGroupJoinRequest;
            $userGroupJoinRequest->setUserGroup($this);
        }

        return $this;
    }

    public function removeUserGroupJoinRequest(UserGroupJoinRequest $userGroupJoinRequest): self
    {
        if ($this->userGroupJoinRequests->contains($userGroupJoinRequest)) {
            $this->userGroupJoinRequests->removeElement($userGroupJoinRequest);
            // set the owning side to null (unless already changed)
            if ($userGroupJoinRequest->getUserGroup() === $this) {
                $userGroupJoinRequest->setUserGroup(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserGroupUser[]
     */
    public function getUserGroupUsers(): Collection
    {
        return $this->userGroupUsers;
    }

    public function addUserGroupUser(UserGroupUser $userGroupUser): self
    {
        if (!$this->userGroupUsers->contains($userGroupUser)) {
            $this->userGroupUsers[] = $userGroupUser;
            $userGroupUser->setUserGroup($this);
        }

        return $this;
    }

    public function removeUserGroupUser(UserGroupUser $userGroupUser): self
    {
        if ($this->userGroupUsers->contains($userGroupUser)) {
            $this->userGroupUsers->removeElement($userGroupUser);
            // set the owning side to null (unless already changed)
            if ($userGroupUser->getUserGroup() === $this) {
                $userGroupUser->setUserGroup(null);
            }
        }

        return $this;
    }
}
