<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserGroupJoinRequestRepository")
 */
class UserGroupJoinRequest
{
    public const REQUEST_STATUS_ACTIVE = 'active';
    public const REQUEST_STATUS_DENIED = 'denied';
    public const REQUEST_STATUS_IN_PROGRESS = 'in_progress';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="userGroupJoinRequests")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\UserGroup", inversedBy="userGroupJoinRequests")
     */
    private $userGroup;

    /**
     * @ORM\Column(type="datetime")
     */
    private $requestDatetime;

    /**
     * @ORM\Column(type="string", length=16)
     */
    private $requestStatus;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getUserGroup(): ?UserGroup
    {
        return $this->userGroup;
    }

    public function setUserGroup(?UserGroup $userGroup): self
    {
        $this->userGroup = $userGroup;

        return $this;
    }

    public function getRequestDatetime(): ?\DateTimeInterface
    {
        return $this->requestDatetime;
    }

    public function setRequestDatetime(\DateTimeInterface $requestDatetime): self
    {
        $this->requestDatetime = $requestDatetime;

        return $this;
    }

    public function getRequestStatus(): ?string
    {
        return $this->requestStatus;
    }

    public function setRequestStatus(string $requestStatus): self
    {
        $this->requestStatus = $requestStatus;

        return $this;
    }
}
