<?php
// src/Entity/User.php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserGroup", mappedBy="owner", orphanRemoval=true)
     */
    private $userGroups;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserGroupJoinRequest", mappedBy="user")
     */
    private $userGroupJoinRequests;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserGroupUser", mappedBy="user")
     */
    private $userGroupUsers;

    public function __construct()
    {
        parent::__construct();
        $this->userGroups = new ArrayCollection();
        $this->userGroupUsers = new ArrayCollection();
        $this->userGroupJoinRequests = new ArrayCollection();
        // your own logic
    }

    /**
     * @return Collection|UserGroup[]
     */
    public function getUserGroups(): Collection
    {
        return $this->userGroups;
    }

    public function addUserGroup(UserGroup $userGroup): self
    {
        if (!$this->userGroups->contains($userGroup)) {
            $this->userGroups[] = $userGroup;
            $userGroup->setOwner($this);
        }

        return $this;
    }

    public function removeUserGroup(UserGroup $userGroup): self
    {
        if ($this->userGroups->contains($userGroup)) {
            $this->userGroups->removeElement($userGroup);
            // set the owning side to null (unless already changed)
            if ($userGroup->getOwner() === $this) {
                $userGroup->setOwner(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserGroupJoinRequest[]
     */
    public function getUserGroupJoinRequests(): Collection
    {
        return $this->userGroupJoinRequests;
    }

    public function addUserGroupJoinRequest(UserGroupJoinRequest $userGroupJoinRequest): self
    {
        if (!$this->userGroupJoinRequests->contains($userGroupJoinRequest)) {
            $this->userGroupJoinRequests[] = $userGroupJoinRequest;
            $userGroupJoinRequest->setUser($this);
        }

        return $this;
    }

    public function removeUserGroupJoinRequest(UserGroupJoinRequest $userGroupJoinRequest): self
    {
        if ($this->userGroupJoinRequests->contains($userGroupJoinRequest)) {
            $this->userGroupJoinRequests->removeElement($userGroupJoinRequest);
            // set the owning side to null (unless already changed)
            if ($userGroupJoinRequest->getUser() === $this) {
                $userGroupJoinRequest->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserGroupUser[]
     */
    public function getUserGroupUsers(): Collection
    {
        return $this->userGroupUsers;
    }

    public function addUserGroupUser(UserGroupUser $userGroupUser): self
    {
        if (!$this->userGroupUsers->contains($userGroupUser)) {
            $this->userGroupUsers[] = $userGroupUser;
            $userGroupUser->setUser($this);
        }

        return $this;
    }

    public function removeUserGroupUser(UserGroupUser $userGroupUser): self
    {
        if ($this->userGroupUsers->contains($userGroupUser)) {
            $this->userGroupUsers->removeElement($userGroupUser);
            // set the owning side to null (unless already changed)
            if ($userGroupUser->getUser() === $this) {
                $userGroupUser->setUser(null);
            }
        }

        return $this;
    }
}