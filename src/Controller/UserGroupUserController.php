<?php

namespace App\Controller;

use App\Entity\UserGroupUser;
use App\Form\UserGroupUserType;
use App\Repository\UserGroupUserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/user-group/users")
 */
class UserGroupUserController extends AbstractController
{
    /**
     * @Route("/", name="user_group_user_index", methods={"GET"})
     */
    public function index(UserGroupUserRepository $userGroupUserRepository): Response
    {
        return $this->render('user_group_user/index.html.twig', [
            'user_group_users' => $userGroupUserRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{id}", name="user_group_user_show", methods={"GET"})
     */
    public function show(UserGroupUser $userGroupUser): Response
    {
        return $this->render('user_group_user/show.html.twig', [
            'user_group_user' => $userGroupUser,
        ]);
    }

    /**
     * @Route("/{id}", name="user_group_user_delete", methods={"DELETE"})
     */
    public function delete(Request $request, UserGroupUser $userGroupUser): Response
    {
        if ($this->isCsrfTokenValid('delete'.$userGroupUser->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($userGroupUser);
            $entityManager->flush();
        }

        return $this->redirectToRoute('user_group_user_index');
    }
}
