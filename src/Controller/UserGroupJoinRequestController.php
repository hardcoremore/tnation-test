<?php

namespace App\Controller;

use App\Entity\UserGroupJoinRequest;
use App\Entity\UserGroup;
use App\Entity\UserGroupUser;
use App\Repository\UserGroupJoinRequestRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/user-group/join-request")
 */
class UserGroupJoinRequestController extends AbstractController
{
    /**
     * @Route("/", name="user_group_join_request_index", methods={"GET"})
     */
    public function index(UserGroupJoinRequestRepository $userGroupJoinRequestRepository): Response
    {
        return $this->render('user_group_join_request/index.html.twig', [
            'user_group_join_requests' => $userGroupJoinRequestRepository->getAllJoinRequestsForUserUserGroupsAndStatus($this->getUser()),
        ]);
    }

    /**
     * @Route("/new/{userGroup}", name="user_group_join_request_new", methods={"GET"})
     */
    public function new(UserGroup $userGroup, Request $request): Response
    {
        if($userGroup->getOwner()->getId() === $this->getUser()->getId()) {
            throw new \Exception('You can not send join request for your own group!');
        }
        else {

            $userGroupJoinRequest = new UserGroupJoinRequest();
            $userGroupJoinRequest->setUserGroup($userGroup);
            $userGroupJoinRequest->setUser($this->getUser());
            $userGroupJoinRequest->setRequestDateTime(new \DateTime());
            $userGroupJoinRequest->setRequestStatus(UserGroupJoinRequest::REQUEST_STATUS_IN_PROGRESS);

            $entityManager = $this->getDoctrine()->getManager();

            $entityManager->persist($userGroupJoinRequest);
            $entityManager->flush();
        }

        return $this->redirectToRoute('user_group_index');
    }

    /**
     * @Route("/show/{id}", name="user_group_join_request_show", methods={"GET"})
     */
    public function show(UserGroupJoinRequest $userGroupJoinRequest): Response
    {
        return $this->render('user_group_join_request/show.html.twig', [
            'user_group_join_request' => $userGroupJoinRequest,
        ]);
    }

    /**
     * @Route("/aproove/{id}", name="user_group_join_request_aproove", methods={"GET"})
     */
    public function aproove(UserGroupJoinRequest $userGroupJoinRequest, Request $request): Response
    {
        // @to-do move this logic to a separate service class
        $entityManager = $this->getDoctrine()->getManager();

        $userGroupJoinRequest->setRequestStatus(UserGroupJoinRequest::REQUEST_STATUS_ACTIVE);

        $entityManager->persist($userGroupJoinRequest);

        $userGroupUser = new UserGroupUser();
        $userGroupUser->setUser($userGroupJoinRequest->getUser());
        $userGroupUser->setUserGroup($userGroupJoinRequest->getUserGroup());
        $userGroupUser->setAccessLevel('all');

        $entityManager->persist($userGroupUser);


        $entityManager->flush();

        return $this->redirectToRoute('user_group_join_request_index');
    }

    /**
     * @Route("/deny/{id}", name="user_group_join_request_deny", methods={"GET"})
     */
    public function deny(UserGroupJoinRequest $userGroupJoinRequest, Request $request): Response
    {
        $userGroupJoinRequest->setRequestStatus(UserGroupJoinRequest::REQUEST_STATUS_DENIED);

        $this->getDoctrine()->getManager()->persist($userGroupJoinRequest);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('user_group_join_request_index');
    }

    /**
     * @Route("/delete/{id}", name="user_group_join_request_delete", methods={"DELETE"})
     */
    public function delete(Request $request, UserGroupJoinRequest $userGroupJoinRequest): Response
    {
        if ($this->isCsrfTokenValid('delete'.$userGroupJoinRequest->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($userGroupJoinRequest);
            $entityManager->flush();
        }

        return $this->redirectToRoute('user_group_join_request_index');
    }
}
