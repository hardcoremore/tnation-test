<?php

namespace App\Repository;

use App\Entity\UserGroupJoinRequest;
use App\Entity\User;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method UserGroupJoinRequest|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserGroupJoinRequest|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserGroupJoinRequest[]    findAll()
 * @method UserGroupJoinRequest[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserGroupJoinRequestRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserGroupJoinRequest::class);
    }

    public function getAllJoinRequestsForUserUserGroupsAndStatus(User $user, string $status = UserGroupJoinRequest::REQUEST_STATUS_IN_PROGRESS):array
    {
        $joinRequests = [];

        foreach ($user->getUserGroups() as $value) {

            foreach ($value->getUserGroupJoinRequests() as $request) {
                if($request->getRequestStatus() === $status) {
                    $joinRequests[] = $request;    
                }
            }
            
        }

        return $joinRequests;
    }

    // /**
    //  * @return UserGroupJoinRequest[] Returns an array of UserGroupJoinRequest objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserGroupJoinRequest
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
